"""An atomic, thread-safe incrementing counter."""

import threading
import time


class AtomicCounter:

    def __init__(self, initial=0):
        """Initialize a new atomic counter to given initial value (default 0)."""
        self.value = initial
        self._lock = threading.Lock()

    def increment(self, num=1):
        """Atomically increment the counter by num (default 1) and return the
        new value.
        """
        with self._lock:
            time.sleep(1)
            self.value = self.value + num
            return self.value

    def get(self):
        with self._lock:
            return self.value
