import json
from datetime import datetime
from flask_pymongo import PyMongo
from .config import Config

mongo = PyMongo()
with open(Config.MARKET_DATA_PATH) as data_file:
    market_data = json.load(data_file)


def get_current_market_data(clock):
    if clock >= len(market_data):
        return False

    current_data = market_data[clock]

    # Update stock prices in db
    for stock_name, data in current_data.items():
        mongo.db.stocks.update_one({'name': stock_name},
                                   {'$set': {'price': data['average']}},
                                   upsert=True)
    return True
