import time
import logging
import threading

from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS

from .database import (mongo, get_current_market_data)
from .atomic_counter import AtomicCounter

atomic_counter = AtomicCounter()


def increment_hack():
    while atomic_counter.get() >= 0 and get_current_market_data(atomic_counter.get()):
        atomic_counter.increment()


def create_app(until_contest=1):
    app = Flask(__name__)
    CORS(app)

    # Config and clean database
    app.config['MONGO_URI'] = "mongodb://localhost:27017/hack"
    mongo.init_app(app)
    # mongo.db.players.drop()
    # mongo.db.stocks.drop()

    # Configure logs
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    # If the atomic counter is negative the contest has not started yet
    threading.Thread(target=increment_hack, args=[]).start()

    from routes.website_api import website_api
    from routes.base_code_api import base_code_api
    app.register_blueprint(website_api, url_prefix='/website_api')
    app.register_blueprint(base_code_api, url_prefix='/base_code_api')

    return app
