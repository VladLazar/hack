import os

class Config(object):
    STARTING_BALANCE = 5000
    MARKET_DATA_PATH = os.environ['MARKET_DATA_PATH']
