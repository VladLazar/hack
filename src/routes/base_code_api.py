from flask import Blueprint
from flask import request
from flask import jsonify

from utils.database import mongo
from utils.database import market_data as dataset
from utils.factory import atomic_counter
from utils.config import Config

base_code_api = Blueprint('base_code_api', __name__)


@base_code_api.route('/initialise', methods=['POST'])
def initialise():
    payload = request.get_json()
    user_name = payload['user_name']

    mongo.db.players.insert_one({
        'name': user_name,
        'balance': Config.STARTING_BALANCE,
        'stocks': {}
    })

    return jsonify({'success': True})


@base_code_api.route('/buy', methods=['POST'])
def buy():
    payload = request.get_json()
    user_name = payload['user_name']
    stock_name = payload['stock_name']
    amount = payload['amount']

    transaction_possible = False

    # Get the price of the stock and the balance of the player
    player_balance = mongo.db.players.find_one({'name': user_name}).get('balance')
    stock_price = mongo.db.stocks.find_one({'name': stock_name}).get('price')

    if stock_price is None:
        return jsonify({'success': transaction_possible})

    if amount * stock_price <= player_balance:
        # Update the balance and the amount of stock for the user
        mongo.db.players.update_one({'name': user_name},
                                    {'$inc': {'balance': -amount * stock_price,
                                              'stocks.' + stock_name: amount}})
        transaction_possible = True

    return jsonify({'success': transaction_possible})


@base_code_api.route('/sell', methods=['POST'])
def sell():
    payload = request.get_json()
    user_name = payload['user_name']
    stock_name = payload['stock_name']
    amount = payload['amount']

    transaction_possible = False

    # Get the price of the stock and the balance of the player
    player_stocks = mongo.db.players.find_one({'name': user_name}).get('stocks').get(stock_name, 0)
    stock_price = mongo.db.stocks.find_one({'name': stock_name}).get('price')

    if amount <= player_stocks:
        mongo.db.players.update_one({'name': user_name},
                                    {'$inc': {'balance': amount * stock_price,
                                              'stocks.' + stock_name: -amount}})
        transaction_possible = True

    return jsonify({'success': transaction_possible})


@base_code_api.route('/game_state', methods=['POST'])
def game_state():
    payload = request.get_json()
    user_name = payload['user_name']

    print(str(payload))

    player_balance = mongo.db.players.find_one({'name': user_name}).get('balance')
    player_stocks = mongo.db.players.find_one({'name': user_name}).get('stocks')

    # Get the price for each stock and place them in a dict.
    stocks = mongo.db.stocks.find()
    market_data = {}
    for stock in stocks:
        market_data.update({stock['name']: stock['price']})

    return jsonify({
        'balance': player_balance,
        'player_stocks': player_stocks,
        'market_data': market_data,
        'percentage': atomic_counter.get() / len(dataset) * 100 
    })
