import pymongo
from flask import jsonify
from flask import Blueprint
from utils.database import mongo


website_api = Blueprint('website_api', __name__)


@website_api.route('/leaderboard', methods=['GET'])
def leaderboard():
    scores = []

    for player in mongo.db.players.find().sort('balance', pymongo.DESCENDING):
        playerDict = {}
        playerDict['rank'] = len(scores) + 1
        playerDict['name'] = player['name']
        playerDict['money'] = player['balance']

        scores.append(playerDict)

    return jsonify(scores)
