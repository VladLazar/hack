# import libraries
import json
import requests

# general data
time = '1d'    # '1d' = one day
interval = '1' # '1' = one minute

# NASDAQ symbols
companies = ["AMZN", "AAPL", "EA", "EBAY", "GOOGL"]

# information to extract
infos = ["volume", "average"] #"date", "minute"

# command to obtain json
# https://api.iextrading.com/1.0/stock/aapl/chart/1d?chartInterval=1

# data for each company
shortDatas = {}
for company in companies:
    # specify the url
    url = 'https://api.iextrading.com/1.0/stock/' + company + '/chart/' + time + '?chartInterval=' + interval
    # specify the params for the request
    params = dict()

    # get data for company
    resp = requests.get(url=url, params=params)
    data = resp.json() # Check the JSON Response Content documentation below

    # write all the data in file for company
    with open('data/' + company + '.json', 'w') as outfile:
        json.dump(data, outfile)

    # extract specific data
    shortData = []
    for item in data:
        currentDict = {}
        for info in infos:
            currentDict[info] = item.get(info)
        shortData.append(currentDict)
    shortDatas[company] = shortData
    
    # write the specified data in file for company
    with open('data/' + company + '-short.json', 'w') as outfile:
        json.dump(shortData, outfile)

# get minimum number of timestamps
size = len(shortDatas.get(companies[0]))
for company in companies:   
    size = min(size, len(shortDatas.get(company)))

# merge data for all companies
# with empty init
finalData = []
for ind in range(0, 5):
    finalData.append({})

for ind in range(0, size // 5):
    currentDict = {}
    for company in companies:
        currentDict[company] = shortDatas.get(company)[ind]

        # fix wrong data
        # by taking last available price
        if currentDict.get(company).get(infos[0]) <= 0 and len(finalData) > 0:
                currentDict[company] = finalData[-1].get(company)

    finalData.append(currentDict)
        
# write the specified data in file for company
with open('data/final-short.json', 'w') as outfile:
    json.dump(finalData, outfile)
print(len(finalData))
