# Pedlar

### For the server
0. sudo service mongod start
1. virtualenv -p /usr/bin/python3 venv
2. source venv/bin/activate
3. pip install -r requirements.txt
4. cd src
5. gunicorn --workers 5 --bind 0.0.0.0:5000 run:app -e MARKET\_DATA\_PATH=path/to/project/sc/scrape/data/final.json

### For the database
1. Change time/interval/companies in the scraper
2. python scraper.py

### For the user
1. Use the Client class from base\_code\_api.py
2. After the server runs, python name\_of\_bot.py
