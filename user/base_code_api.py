import requests
import json
import time

class Client(object):
    def __init__(self, user_input):
        self.server = 'http://35.246.18.184:5000/base_code_api'
        self.user_name = str(user_input)
        self.entered = False
        self.__initialise()

    def __initialise(self):
        url = self.server + '/initialise'
        data = {'user_name': self.user_name}

        # get response
        done = False
        while not done:
            try:
                done = True
                resp = requests.post(url, json=data)
                resp_json = resp.json()
            except:
                done = False

        print("The game has started!")
        return True
    
    def buy(self, stock_name, amount):
        url = self.server + '/buy'
        data = {'user_name': self.user_name, 'stock_name': stock_name, 'amount': amount}

        # get response
        resp = requests.post(url, json=data)
        resp_json = resp.json()

        return resp_json.get('success')

    def sell(self, stock_name, amount):
        url = self.server + '/sell'
        data = {'user_name': self.user_name, 'stock_name': stock_name, 'amount': amount}

        # get response
        resp = requests.post(url, json=data)
        resp_json = resp.json()

        return resp_json.get('success')
    
    def game_state(self):
        url = self.server + '/game_state'
        data = {'user_name': self.user_name}

        # get response
        resp = requests.post(url, json=data)

        self.__stats(resp.json())
        if resp.json().get('percentage') < 100:
            return resp.json()
        else:
            return None

    def __stats(self, data):
        percentage = data.get('percentage')
        if self.entered:
            return
        if(percentage < 100):
            print("The game is " + str(percentage) + "% completed!")
        else:
            self.entered = True
            print("The game has completed!")

        balance = data.get('balance')
        stocks = data.get('player_stocks')
        print("Your balance is " + str(balance))
        print("Your stocks are " + str(stocks))


