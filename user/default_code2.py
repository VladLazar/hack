import sys
import random
from base_code_api import Client

user_name = "Bob"

# buy low, sell high for Google: as much as possible (position is high)
def basic_algorithm(client):
    companies = ["AMZN", "AAPL", "EA", "EBAY", "GOOGL"]
    stock_name = companies[random.randint(0, len(companies) - 1)]

    current_stocks = {stock_name: None}
    previous_state = None

    try:
        current_state = client.game_state()
    except:
        print("The game could not start for you!")
        sys.exit()
    while current_state != None:

        # skip first round
        if previous_state == None:
            previous_state = current_state
            continue

        stocks_info = current_stocks.get(stock_name)
        current_price = current_state.get('market_data').get(stock_name)
        previous_price = previous_state.get('market_data').get(stock_name)

        # buy low
        if stocks_info == None:
            if current_price < previous_price and current_state.get('percentage') < 90:
                possible_stocks = int(current_state.get('balance') / current_price)
                # check if valid
                if client.buy(stock_name, possible_stocks):
                    current_stocks[stock_name] = [possible_stocks, current_price]
                    print("Bought " + str(possible_stocks) + " " + stock_name + " for " + str(current_price))
        else:  # sell high
            bought_price = stocks_info[1]
            if current_price > bought_price or current_state.get('percentage') > 95:
                if client.sell(stock_name, stocks_info[0]):
                    current_stocks[stock_name] = None
                    print("Sold " + str(possible_stocks) + " " + stock_name + " for " + str(current_price))

        previous_state = current_state
        try:
            current_state = client.game_state()
        except:
            print("The game has finished for you!")

            balance = previous_state.get('balance')
            stocks = previous_state.get('player_stocks')
            print("Your balance is " + str(balance))
            print("Your stocks are " + str(stocks))
            sys.exit()

def main():
    client = Client(user_name)
    basic_algorithm(client)

if __name__ == "__main__":
    main()
